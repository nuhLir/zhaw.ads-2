package ch.zhaw.ads.competition;

import ch.zhaw.ads.tree.SortedBinaryTree;
import ch.zhaw.ads.tree.Tree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CompetitorLoader {
    private List<Competitor> competitors;

    public CompetitorLoader(){
        this.competitors = new ArrayList<>();
        this.loadCompetitorData();
    }

    public List<Competitor> getCompetitorList(){
        return this.competitors;
    }

    public void sortByRank (List<Competitor> competitors){
        sortByDelegate(competitors, new CompetitorRankComparer());
    }

    public void sortByName(List<Competitor> competitors){
        sortByDelegate(competitors, new CompetitorNameComparer());
    }

    private void sortByDelegate(List<Competitor> competitors, CompetitorComparator delegate){
        for (Competitor item : competitors){
            item.setComparisonHandler(delegate);
        }

        Collections.sort(competitors);
    }

    private void loadCompetitorData() {
        String csvFile = "res/Marathon.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        try {
            br = new BufferedReader(new FileReader(csvFile));

            while ((line = br.readLine()) != null) {

                // use ; as separator
                String[] competitor = line.split(cvsSplitBy);

                competitors.add(new Competitor(Integer.parseInt(competitor[0]), competitor[1], Integer.parseInt(competitor[2]), competitor[3], competitor[4]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Competitor> loadAll(String fileName) throws IOException {
        String[] lines = fileLines(fileName);
        List<Competitor> result = new ArrayList<>();

        for (String line : lines) {
            String[] properties = line.split(";");

            Competitor competitor = new Competitor(
                    Integer.parseInt(properties[0]),
                    properties[1],
                    Integer.parseInt(properties[2]),
                    properties[3],
                    properties[4]
            );

            result.add(competitor);
        }

        return result;
    }

    public Tree<Competitor> loadAllIntoTree(String fileName) throws IOException {
        Tree<Competitor> tree = new SortedBinaryTree<>();
        CompetitorComparator delegate = new CompetitorRankComparer();

        for (Competitor competitor : loadAll(fileName)){
            competitor.setComparisonHandler(delegate);
            tree.add(competitor);
        }

        return tree;
    }

    private String[] fileLines(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);

        List<String> lines;

        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }

        return lines.toArray(new String[lines.size()]);
    }
}
