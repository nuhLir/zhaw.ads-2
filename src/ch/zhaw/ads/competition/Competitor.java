package ch.zhaw.ads.competition;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Competitor implements Comparable<Competitor> {

    private String name;
    private String country;
    private long time;
    private int jg;
    private int startNr;
    private int rank;
    private CompetitorComparator comparisonHandler = null;

    public Competitor(int startNr, String name, int jg, String country, String time) {
        this.startNr = startNr;
        this.name = name;
        this.jg = jg;
        this.country = country;
        try{
            this.time = parseTime(time);
        }catch(Exception e){
            System.err.println("Wrong Dateformat supplied!");
        }
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public int getJg() {
        return jg;
    }

    public int getStartNr(){
        return startNr;
    }

    private static long parseTime(String s) throws ParseException {
        DateFormat sdf = new SimpleDateFormat("HH:mm:ss.S");
        Date date = sdf.parse(s);
        return date.getTime();
    }

    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.S");
        StringBuilder sb = new StringBuilder();
        sb.append(rank);
        sb.append(" ");
        sb.append(name);
        sb.append(" ");
        sb.append(Integer.toString(jg));
        sb.append(" ");
        sb.append(df.format(new Date(time)));
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Competitor)){
            return false;
        }

        return (this == obj);
    }

    @Override
    public int hashCode() {
        int hash = 3;

        hash = 53 * hash + Objects.hashCode(name);
        hash = 53 * hash + Objects.hashCode(country);
        hash = 53 * hash + jg;
        hash = 53 * hash + startNr;

        return hash;
    }
    
    @Override
    public int compareTo(Competitor o) {
        if (comparisonHandler == null){
            return 0;
        } else {
            return comparisonHandler.compare(this, o);
        }
    }

    public void setComparisonHandler(CompetitorComparator comparisonHandler) {
        this.comparisonHandler = comparisonHandler;
    }
}
