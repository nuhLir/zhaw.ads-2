package ch.zhaw.ads.competition;

public class CompetitorNameComparer implements CompetitorComparator {
    @Override
    public int compare(Competitor self, Competitor other) {
        int nameComparison = self.getName().compareTo(other.getName());

        if (nameComparison == 0){
            return new Integer(self.getJg()).compareTo(new Integer(other.getJg()));
        }

        return nameComparison;
    }
}
