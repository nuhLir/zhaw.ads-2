package ch.zhaw.ads.competition;


public interface CompetitorComparator {
    int compare(Competitor self, Competitor other);
}
