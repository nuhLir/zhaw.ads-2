package ch.zhaw.ads.competition;

public class CompetitorRankComparer implements CompetitorComparator {
    @Override
    public int compare(Competitor self, Competitor other) {
        if (other.getTime() > self.getTime()){
            return -1;
        }

        if (other.getTime() < self.getTime()){
            return 1;
        }

        return 0;
    }
}
