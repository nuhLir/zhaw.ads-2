package ch.zhaw.ads.graph;

import java.util.*;

public class AdjListGraph<N extends Node, E extends Edge>
        implements Graph<N, E> {

    private final List<N> nodes = new LinkedList<>();
    private final Class nodeClazz;
    private final Class edgeClazz;

    public AdjListGraph(Class nodeClazz, Class edgeClazz) {
        this.nodeClazz = nodeClazz;
        this.edgeClazz = edgeClazz;
    }

    @Override
    public N addNode(String name) throws Throwable {
        N node = findNode(name);
        if (node == null) {
            node = (N) nodeClazz.newInstance();
            node.setName(name);
            nodes.add(node);
        }
        return node;
    }

    @Override
    public void addEdge(String source, String dest, double weight) throws Throwable {
        N src = addNode(source);
        N dst = addNode(dest);

        try {
            E edge = (E) edgeClazz.newInstance();
            edge.setDest(dst);
            edge.setWeight(weight);
            src.addEdge(edge);
        } catch (InstantiationException | IllegalAccessException e) {
        }
    }

    @Override
    public N findNode(String name) {
        for (N node : nodes) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    @Override
    public Iterable<N> getNodes() {
        return nodes;
    }
}
