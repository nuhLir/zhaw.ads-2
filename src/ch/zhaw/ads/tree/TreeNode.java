package ch.zhaw.ads.tree;

public class TreeNode<T extends Comparable<T>> {
    public T element;
    public TreeNode left, right;
    public int height;

    public TreeNode(T element) {
        this.element = element;
    }

    public TreeNode(T element, TreeNode left, TreeNode right) {
        this(element);
        this.left = left;
        this.right = right;
    }

    public T getValue() {
        return element;
    }

    public void setRight(TreeNode node){
        right = node;
    }

    public T getElement(){
        return element;
    }

    public void setLeft(TreeNode node){
        left = node;
    }

    public TreeNode getLeft() {
        return left;
    }

    public TreeNode getRight() {
        return right;
    }
}