package ch.zhaw.ads.tree;

public class SortedBinaryTree<T extends Comparable<T>> implements Tree<T> {

    private TreeNode<T> root;
    private T removed;

    private TreeNode<T> insertAt(TreeNode<T> node, T x) {
        if (node == null) {
            return new TreeNode<>(x);
        } else {
            if (x.compareTo(node.getElement()) <= 0) {
                node.setLeft(insertAt(node.getLeft(), x));
            } else {
                node.setRight(insertAt(node.getRight(), x));
            }
            return node;
        }
    }

    public TreeNode<T> getRoot(){
        return root;
    }

    @Override
    public void add(T x) {
        root = insertAt(root, x);
    }

    // find node to replace
    TreeNode<T> rep;

    private TreeNode<T> findRepAt(TreeNode<T> node) {
        if (node.getRight() != null) {
            node.setRight(findRepAt(node.getRight()));
            return node;
        } else {
            rep = node;
            return node.getLeft();
        }
    }

    // remove node
    private TreeNode<T> removeAt(TreeNode<T> node, T x) {
        if (node == null) {
            return null;
        } else {
            if (x.compareTo(node.getElement()) == 0) {
                // found
                removed = node.getElement();
                if (node.getLeft() == null) {
                    return node.getRight();
                } else if (node.getRight() == null) {
                    return node.getLeft();
                } else {
                    node.setLeft(findRepAt(node.getLeft()));
                    rep.setLeft(node.getLeft());
                    rep.setRight(node.getRight());
                    return rep;
                }
            } else if (x.compareTo(node.getElement()) <= 0) // search left
            {
                node.setLeft(removeAt(node.getLeft(), x));
            } else // search right
            {
                node.setRight(removeAt(node.getRight(), x));
            }
            return node;
        }
    }

    @Override
    public T remove(T x) {
        removed = null;
        root = removeAt(root, x);
        return removed;
    }

    @Override
    public T removeLast() {
        if (root.getRight() != null) {
            root.setRight(findRepAt(root.getRight()));
        } else {
            rep = root;
            root = root.getRight();
        }
        return rep.getElement();
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public Traversal<T> traversal() {
        return new TreeTraversal<>(root);
    }
}