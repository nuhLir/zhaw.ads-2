package ch.zhaw.ads.tree;

import ch.zhaw.ads.utils.MyQueue;

public class TreeTraversal<T extends Comparable<T>> implements Traversal<T> {
    private final TreeNode<T> root;

    public TreeTraversal(TreeNode<T> root) {
        this.root = root;
    }

    @Override
    public void inorder(Visitor<T> visitor) {
        if (root != null) {
            inorder(root, visitor);
        }
    }

    private void inorder(TreeNode node, Visitor visitor) {
        if (node.getLeft() != null) {
            inorder(node.getLeft(), visitor);
        }

        visitor.visit(node.getElement());

        if (node.getRight() != null) {
            inorder(node.getRight(), visitor);
        }
    }

    @Override
    public void preorder(Visitor<T> visitor) {
        if (root != null) {
            preorder(root, visitor);
        }
    }

    private void preorder(TreeNode node, Visitor visitor) {
        visitor.visit(node.getElement());

        if (node.getLeft() != null) {
            preorder(node.getLeft(), visitor);
        }

        if (node.getRight() != null) {
            preorder(node.getRight(), visitor);
        }
    }

    @Override
    public void postorder(Visitor<T> visitor) {
        if (root != null) {
            postorder(root, visitor);
        }
    }

    private void postorder(TreeNode node, Visitor visitor) {
        if (node.getLeft() != null) {
            postorder(node.getLeft(), visitor);
        }

        if (node.getRight() != null) {
            postorder(node.getRight(), visitor);
        }

        visitor.visit(node.getElement());
    }

    @Override
    public void levelorder(Visitor vis) {
        MyQueue<TreeNode> queue = new MyQueue<>();
        TreeNode node = root;
        if (root != null) {
            queue.add(node);
        }
        while (!queue.isEmpty()) {
            node = queue.dequeue();
            vis.visit(node.getElement());

            if (node.getLeft() != null) {
                queue.add(node.getLeft());
            }
            if (node.getRight() != null) {
                queue.add(node.getRight());
            }
        }

    }

}