package ch.zhaw.ads.maze;

import ch.zhaw.ads.graph.Edge;

public class MazeEdge<N> extends Edge<N> {
    private boolean isDeadend = false;

    public MazeEdge(N dest, double weight) {
        super(dest, weight);
    }

    public boolean isDeadend(){
        return this.isDeadend;
    }

    public void setDeadend(){
        this.isDeadend = true;
    }
}
