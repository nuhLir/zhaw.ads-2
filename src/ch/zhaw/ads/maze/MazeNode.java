package ch.zhaw.ads.maze;

import ch.zhaw.ads.graph.Node;

public final class MazeNode<T> extends Node<T> {
    private int x;
    private int y;
    private boolean mark = false;
    
    public MazeNode(){
    }
    
    public MazeNode(int x, int y){
        setX(x);
        setY(y);
    }
    
    public void setX(int x){
        this.x = x;
    }
    
    public void setY(int y){
        this.y = y;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }

    public boolean isMarked() {
        return mark;
    }

    public void markNode() {
        this.mark = true;
    }

    public void unmarkNode() {
        this.mark = false;
    }
}
