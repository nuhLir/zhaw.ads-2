package ch.zhaw.ads.maze;

import ch.zhaw.ads.graph.Graph;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;


public class MazePanel extends JPanel {

    private Graph<MazeNode, MazeEdge> graph;

    // --------------------------------------------------
    // DEFINE THE MAZE BELOW AND SET START AND ENDPOINTS
    // --------------------------------------------------

    public static final String MAZE_FILE = "res/Labyrinth2.txt";
    public static final String START_MAZE = "0-1";
    public static final String END_MAZE = "7-9";

    public MazePanel() {
        super();

        if (graph == null) {
            try {
                graph = new MazeManager().load(MAZE_FILE);
            } catch (IOException ex) {
                Logger.getLogger(MazePanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void doDrawing(Graphics g) {
        if (graph == null) {
            return;
        }

        Graphics2D g2d = (Graphics2D) g;

        g2d.setPaint(Color.blue);

        // fetch first node and start looping
        MazeNode<MazeEdge> node = null;
        Iterator<MazeNode> iterator = graph.getNodes().iterator();

        if (iterator.hasNext()) {
            node = iterator.next();
        }

        while (node != null) {
            MazeNode<MazeEdge> nextNode = null;

            for (MazeEdge<MazeNode> next : node.getEdges()) {
                nextNode = next.getDest();

                g2d.drawLine(
                        coordinate(node.getX()),
                        coordinate(node.getY()),
                        coordinate(nextNode.getX()),
                        coordinate(nextNode.getY())
                );
            }

            node = nextNode;
            if (node == null && iterator.hasNext()) {
                node = iterator.next();
            }
        }

        MazeNode<MazeEdge> start = this.graph.findNode(START_MAZE);
        MazeNode<MazeEdge> end = this.graph.findNode(END_MAZE);
        if (computePath(start, end)) {
            System.out.println("Path found!");

            g2d.setPaint(Color.red);
            node = start;
            while(node != end){
                MazeNode<MazeEdge> destNode = null;
                for (MazeEdge<MazeNode> edge : node.getEdges()) {
                    if(edge.getDest().isMarked()){
                        destNode = edge.getDest();
                        g2d.drawLine(
                                coordinate(node.getX()),
                                coordinate(node.getY()),
                                coordinate(destNode.getX()),
                                coordinate(destNode.getY())
                        );
                    }
                }
                node = destNode;
            }
        }else{
            System.err.println("No path found!");
            return;
        }

    }

    private int coordinate(int input) {
        return input * 50 + 20;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }

    private void debugGraph() {
        for (MazeNode<MazeEdge> node : graph.getNodes()) {
            System.out.println("--- \t " + node.getName());
            System.out.println("\t Edge List Size: " + ((LinkedList<MazeEdge>) (node.getEdges())).size());
            System.out.println("\t Marked: " + node.isMarked());
            for (MazeEdge<MazeNode> edge : node.getEdges()) {
                System.out.println("\t\t Edge to: " + edge.getDest().getName());
            }
        }
    }

    private boolean computePath(MazeNode<MazeEdge> node, MazeNode<MazeEdge> target) {
        node.markNode();
        if(isNodeSame(node, target)){
            return true;
        }else{
            for(MazeEdge<MazeNode> edge : node.getEdges()){
                MazeNode<MazeEdge> tmp = edge.getDest();
                if(!tmp.isMarked()){
                    if(computePath(tmp, target)) return true;
                }
            }
        }
        node.unmarkNode();
        return false;
    }

    private boolean isNodeSame(MazeNode<MazeEdge> one, MazeNode<MazeEdge> two) {
        return one.getName().equals(two.getName());
    }

}
