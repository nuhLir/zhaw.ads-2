package ch.zhaw.ads.maze;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class MazeRenderer extends JFrame {
    public MazeRenderer() {
        initUi();
    }

    private void initUi() {
        setTitle("Maze Renderer");
        setSize(480, 480);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        add(new MazePanel());
    }

    /**
     * Static main method
     *
     * @param args
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                MazeRenderer ex = new MazeRenderer();
                ex.setVisible(true);
            }
        });
    }
}
