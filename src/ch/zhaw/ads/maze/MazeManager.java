package ch.zhaw.ads.maze;

import ch.zhaw.ads.graph.AdjListGraph;
import ch.zhaw.ads.graph.Graph;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MazeManager {

    public Graph<MazeNode, MazeEdge> load(String fileName) throws IOException {
        Graph<MazeNode, MazeEdge> graph = new AdjListGraph<>(MazeNode.class, MazeEdge.class);

        for (String line : fileLines(fileName)) {
            String[] tokens = line.replaceAll("\\s+", "-").split("-");

            try {

                MazeNode<MazeEdge> first = addNode(tokens[0], tokens[1], graph);
                MazeNode<MazeEdge> second = addNode(tokens[2], tokens[3], graph);

                first.addEdge(new MazeEdge<MazeNode>(second, 0));
            } catch (Throwable ex) {
                Logger.getLogger(MazeManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return graph;
    }

    private MazeNode<MazeEdge> addNode(String firstToken, String secondToken, Graph<MazeNode, MazeEdge> graph) throws Throwable {
        String name = String.format("%s-%s", firstToken, secondToken);

        MazeNode<MazeEdge> lookup = graph.findNode(name);

        if (lookup != null) {
            return lookup;
        }

        MazeNode<MazeEdge> node = graph.addNode(name);

        node.setX(Integer.parseInt(firstToken));
        node.setY(Integer.parseInt(secondToken));

        return node;
    }

    private String[] fileLines(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);

        List<String> lines = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }

        return lines.toArray(new String[lines.size()]);
    }
}
