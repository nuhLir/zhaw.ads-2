package ch.zhaw.ads.simulatedannealing;

public class Circle {
    double centerX, centerY;
    double r;
    double phi1, phi2;

    public Circle(double cX, double cY, double r, double phi1, double phi2) {
        centerX = cX;
        centerY = cY;
        this.r = r;
        this.phi1 = phi1;
        this.phi2 = phi2;
    }
}