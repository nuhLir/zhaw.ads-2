package ch.zhaw.ads.sort;

import java.util.Random;

public class SortDemo {
    public static void main(String[] args) {
        SortDemo sortDemo = new SortDemo();
        Integer[] data = sortDemo.generateIntegerDataset(10);
        SortAlgorithm sa = new QuickSort(data);
        Sorter sorter = new Sorter<>(sa);
        sortDemo.outData((Integer[]) sorter.run());
        System.out.println("Execution Time: " + sorter.getAlgorithmExecutionTimeInMs() + "ms");
        System.out.println("Execution Time: " + sorter.getAlgorithmExecutionTimeInNs() + "ns");
    }

    private void outData(Integer[] sortedData) {
        for(Integer el : sortedData){
            System.out.println(el);
        }
    }

    public Integer[] generateIntegerDataset(int dataAmount){
        Integer[] data = new Integer[dataAmount];
        Random rand = new Random();
        for(int i = 0; i < dataAmount; i++){
            Integer value = null;
            if(data instanceof Integer[]){
                value = new Integer(rand.nextInt(((1000000 - 1000) + 1) + 1000));
            }
            data[i] = value;
        }
        return data;
    }


}
