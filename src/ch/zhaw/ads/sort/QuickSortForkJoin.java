package ch.zhaw.ads.sort;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

public class QuickSortForkJoin extends RecursiveAction {
    private static final int SPLIT_THRESHOLD = 55;
    private Integer[] data = null;
    private int left, right;

    public QuickSortForkJoin(Integer[] data, int left, int right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public static Integer[] sort(Integer[] data) {
        int parallelism = java.lang.Runtime.getRuntime().availableProcessors();
        ForkJoinPool forkJoinPool = new ForkJoinPool(parallelism);
        QuickSortForkJoin rootTask = new QuickSortForkJoin(data, 0, data.length - 1);
        forkJoinPool.invoke(rootTask);
        return rootTask.getDataset();
    }

    public Integer[] getDataset(){
        return this.data;
    }


    private void quickSort(Integer[] data, int left, int right){
        ForkJoinTask t1 = null;
        ForkJoinTask t2 = null;
        int r = partition(data, left, right);
        if ((r - left) > SPLIT_THRESHOLD) {
            t1 = new QuickSortForkJoin(data, left, r).fork();
            t2 = new QuickSortForkJoin(data, r + 1, right).fork();
        }else{
            Arrays.sort(data);
        }
        if ((right - r) > SPLIT_THRESHOLD) {
            t1.invoke();
            t2.invoke();
        }else{
            Arrays.sort(data);
        }
        if(t1 != null){
            t1.join();
        }
        if(t2 != null){
            t2.join();
        }

    }

    @Override
    protected void compute() {
        quickSort(this.data, this.left, this.right);
    }

    private int partition(Integer[] data, int left, int right){
        int pivot = data[left];
        int i = left - 1;
        int j = right + 1;
        while(true){
            do{
                j--;
            }while(data[j] > pivot);
            do{
                i++;
            }while(data[i] < pivot);
            if(i < j){
                int temp = data[i];
                data[i] = data[j];
                data[j] = temp;
            }else{
                return j;
            }
        }
    }
}
