package ch.zhaw.ads.sort;

public abstract class SortAlgorithm<T extends Comparable<? super T>> {
    protected T[] data = null;
    protected long start, end;
    protected int datasetLength;

    public SortAlgorithm(T[] dataset){
        this.data = dataset;
        this.datasetLength = this.data.length;
    }

    protected void setStart() {
        //this.start = System.currentTimeMillis();
        this.start = System.nanoTime();
    }

    protected void setEnd() {
        //this.end = System.currentTimeMillis();
        this.end = System.nanoTime();
    }

    public long execute() {
        sort();
        return getExecutionTime();
    }

    public T[] getDataSet() {
        return this.data;
    }

    protected void sort() {
        /*
        T[] workingData = (T[]) getCopyOfDataset();
        setStart();
            //Implementation goes here
        setEnd();
        this.data = workingData;
        */
    }

    private long getExecutionTime() {
        return this.end - this.start;
    }

    protected T[] getCopyOfDataset(){
        Class<?> arrayType = this.data.getClass().getComponentType();
        T[] workingData = (T[])java.lang.reflect.Array.newInstance(arrayType, this.datasetLength);
        System.arraycopy(this.data, 0, workingData, 0, this.datasetLength);
        return  workingData;
    }
}
