package ch.zhaw.ads.sort;

import java.util.Random;

public class QuickSortForkJoinDemo {
    public static void main(String[] args) {
        QuickSortForkJoinDemo qsfjd = new QuickSortForkJoinDemo();
        Integer[] dataSet = qsfjd.generateIntegerDataset(1000);
        System.out.println("--------------------------- Unsorted");
        QuickSortForkJoinDemo.outData(dataSet);
        System.out.println("--------------------------- Sorted");
        Integer[] sorted = QuickSortForkJoin.sort(dataSet);
        QuickSortForkJoinDemo.outData(sorted);
    }

    public static void outData(Integer[] data) {
        for(Integer el : data){
            System.out.println(el);
        }
    }

    public Integer[] generateIntegerDataset(int dataAmount){
        Integer[] data = new Integer[dataAmount];
        Random rand = new Random();
        for(int i = 0; i < dataAmount; i++){
            Integer value = null;
            if(data instanceof Integer[]){
                value = new Integer(rand.nextInt(((1000000 - 1000) + 1) + 1000));
            }
            data[i] = value;
        }
        return data;
    }
}
