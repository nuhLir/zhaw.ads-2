package ch.zhaw.ads.sort;


public class InsertionSort<T extends Comparable<? super T>> extends SortAlgorithm {
    public InsertionSort(T[] dataset) {
        super(dataset);
    }

    @Override
    protected void sort() {
        T[] workingData = (T[]) getCopyOfDataset();
        setStart();
        for (int a = 1; a < this.datasetLength; ++a) {
            for (int b = a; b > 0; --b) {
                if (workingData[b - 1].compareTo(workingData[b]) > 0) {
                    T temp = workingData[b - 1];
                    workingData[b - 1] = workingData[b];
                    workingData[b] = temp;
                }
            }
        }
        setEnd();
        this.data = workingData;
    }
}
