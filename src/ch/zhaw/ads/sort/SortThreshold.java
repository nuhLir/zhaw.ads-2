package ch.zhaw.ads.sort;

import java.util.Arrays;
import java.util.Random;

public class SortThreshold {

    public static void main(String[] args) {
        SortThreshold sortThreshold = new SortThreshold();
        System.out.println("\"Dataset Size\"\t\"Execution Time (ns)\"");
        for(int i = 0; i < 150; i++){
            Integer[] data = sortThreshold.generateIntegerDataset(i);
            SortAlgorithm sa = new QuickSort(data);
            Sorter sorter = new Sorter<>(sa);
            sorter.run();
            System.out.println(i+"\t"+sorter.getAlgorithmExecutionTimeInNs());
        }
        System.out.println("Array Sort");
        System.out.println("\"Dataset Size\"\t\"Execution Time (ns)\"");
        for(int i = 0; i < 150; i++){
            Integer[] data = sortThreshold.generateIntegerDataset(i);
            long start = System.nanoTime();
            Arrays.sort(data);
            long end = System.nanoTime();
            System.out.println(i+"\t"+(end-start));
        }
    }
    public Integer[] generateIntegerDataset(int dataAmount){
        Integer[] data = new Integer[dataAmount];
        Random rand = new Random();
        for(int i = 0; i < dataAmount; i++){
            Integer value = null;
            if(data instanceof Integer[]){
                value = new Integer(rand.nextInt(((1000000 - 1000) + 1) + 1000));
            }
            data[i] = value;
        }
        return data;
    }
}
