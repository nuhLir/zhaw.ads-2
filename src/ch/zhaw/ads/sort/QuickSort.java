package ch.zhaw.ads.sort;

import java.util.Arrays;

public class QuickSort<T extends Comparable<? super T>> extends SortAlgorithm{

    private static int THRESHOLD = 55;

    public QuickSort(T[] dataset) {
        super(dataset);
    }

    @Override
    protected void sort() {
        T[] workingData = (T[]) getCopyOfDataset();
        setStart();
        quickSort(workingData, 0, workingData.length-1);
        setEnd();
        this.data = workingData;
    }

    private void quickSort(T[] data, int left, int right){
        if(right - left < QuickSort.THRESHOLD){
            Arrays.sort(data);
        }else{
            int median = partition(data, left, right);
            quickSort(data, left, median);
            quickSort(data, median + 1, right);
        }
    }

    private int partition(T[] data, int left, int right){
        T pivot = data[left];
        int i = left - 1;
        int j = right + 1;
        while(true){
            do{
                j--;
            }while(data[j].compareTo(pivot) == 1);
            do{
                i++;
            }while(data[i].compareTo(pivot) == -1);
            if(i < j){
                T temp = data[i];
                data[i] = data[j];
                data[j] = temp;
            }else{
                return j;
            }
        }
    }
}
