package ch.zhaw.ads.sort;

public class BubbleSort<T extends Comparable<? super T>> extends SortAlgorithm {

    public BubbleSort(T[] dataset) {
        super(dataset);
    }

    @Override
    protected void sort() {
        T[] workingData = (T[]) getCopyOfDataset();
        setStart();
        for (int a = 0; a < this.datasetLength - 1; a++) {
            if (workingData[a].compareTo(workingData[a + 1]) > 0) {
                T tmp = workingData[a];
                workingData[a] = workingData[a + 1];
                workingData[a + 1] = tmp;
            }
        }
        setEnd();
        this.data = workingData;
    }
}
