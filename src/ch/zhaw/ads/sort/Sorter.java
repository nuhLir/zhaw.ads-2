package ch.zhaw.ads.sort;

import java.util.concurrent.TimeUnit;

public class Sorter<T extends Comparable<? super T>> {
    private long executionTime = 0;

    private SortAlgorithm<T> algorithm = null;

    public Sorter(SortAlgorithm algorithm){
        this.algorithm = algorithm;
    }

    public T[] run(){
        this.executionTime = this.algorithm.execute();
        return this.algorithm.getDataSet();
    }

    public long getAlgorithmExecutionTimeInNs(){
        return this.executionTime;
    }

    public long getAlgorithmExecutionTimeInMs(){
        return TimeUnit.MILLISECONDS.convert(this.executionTime, TimeUnit.NANOSECONDS);
    }
}
