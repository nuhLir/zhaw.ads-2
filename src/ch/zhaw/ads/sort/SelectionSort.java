package ch.zhaw.ads.sort;

public class SelectionSort<T extends Comparable<? super T>> extends SortAlgorithm {
    public SelectionSort(T[] dataset) {
        super(dataset);
    }

    @Override
    protected void sort() {
        T[] workingData = (T[]) getCopyOfDataset();
        setStart();
        for (int a = 0; a < this.datasetLength - 1; a++) {
            int aSmallest = a;
            for (int b = a + 1; b < this.datasetLength; b++) {
                if (workingData[aSmallest].compareTo((workingData[b])) > 0) {
                    aSmallest = b;
                }
            }
            T aSwap = workingData[aSmallest];
            workingData[aSmallest] = workingData[a];
            workingData[a] = aSwap;
        }
        setEnd();
        this.data = workingData;
    }
}
