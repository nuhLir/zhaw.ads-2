package ch.zhaw.ads.search;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpamBot {
    List<String> data = null;
    List<String> email = null;
    private static String regexEmail = "(([a-zA-Z0-9\\.\\-_])+(@|\\(at\\))([a-zA-Z0-9\\.\\-_])+\\.[a-zA-Z]+)";

    public static void main(String[] args) {
        new SpamBot("https://www.zhaw.ch/en/sozialearbeit/ueber-uns/campus-toni-areal/");
    }

    public SpamBot(String url){
        this.data = new ArrayList<>();
        this.email = new ArrayList<>();
        try {
            crawl(url);
            parse();
            output();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void output() {
        for (String email : this.email) {
            System.out.println(email);
        }
    }

    private void parse() {
        Pattern emailPattern = Pattern.compile(regexEmail, Pattern.CASE_INSENSITIVE);
        Matcher matcher = null;
        for (String d : this.data) {
            matcher = emailPattern.matcher(d);
            int count = 0;
            while(matcher.find()){
                this.email.add(d.substring(matcher.start(count), matcher.end(count)));
                count++;
            }
        }
    }

    private void crawl(String url) throws IOException {
        URL site = new URL(url);
        BufferedReader in = new BufferedReader( new
                InputStreamReader(site.openStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            this.data.add(inputLine);
        }
        in.close();
    }
}
