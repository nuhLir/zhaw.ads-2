package ch.zhaw.ads.search;

public class PraktikumAufgabeEinsUndZwei {
    public static void main(String[] args) {
        /** LEVENSHTEIN */
        System.out.println("Levenshtein: ");
        String a, b;
        a = "AUSTAUSCH";
        b = "AUFBAUSCH";
        System.out.println(a + " - " + b + " : "+ PraktikumAufgabeEinsUndZwei.levenshtein(a,b));
        a = "BARBAREN";
        b = "BARBARA";
        System.out.println(a + " - " + b + " : "+ PraktikumAufgabeEinsUndZwei.levenshtein(a,b));
        a = "COCACOLA";
        b = "COCAINA";
        System.out.println(a + " - " + b + " : "+ PraktikumAufgabeEinsUndZwei.levenshtein(a,b));

        /** REGEX */
        System.out.println("REGEX Beispiele Teil A: ");
        String regex, description;
        regex = "a?b+";
        description = "ab, abb, abbb, ... , b, bb, bbb, ...";
        System.out.println(regex + "\t -> \t" + description);
        regex = "ein(e|er)?";
        description = "ein, eine, einer";
        System.out.println(regex + "\t -> \t" + description);
        regex = "a(x|y)?b*";
        description = "a, ax, ay, ab, axb, ayb, abb, axbb...";
        System.out.println(regex + "\t -> \t" + description);
        regex = "(0*1*)*";
        description = "Binär-Zahlen";
        System.out.println(regex + "\t -> \t" + description);
        regex = "[0-9]*";
        description = "Ganze Zahl";
        System.out.println(regex + "\t -> \t" + description);
        System.out.println("Teil B: ");
        regex = "(25[0-5])|(2[0-5][0-9])|([0-1]?[0-9]?[0-9])\\.(25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])\\.(25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])\\.(25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])";
        description = "IP - Addressen";
        System.out.println(regex + "\t -> \t" + description);
        regex = "(.)+((\\@)|(\\(at\\)))(.)+\\.[a-z]{1,3}";
        description = "E-Mail";
        System.out.println(regex + "\t -> \t" + description);
        regex = "^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*" +
                "@[a-zA-Z](-?[a-zA-Z0-9])*(\\.[a-zA-Z](-?[a-zA-Z0-9])*)+$";
        description = "E-Mail";
        System.out.println(regex + "\t -> \t Exact " + description);

    }

    public static int levenshtein(String a, String b){
        a = a.toLowerCase();
        b = b.toLowerCase();
        // i == 0
        int [] costs = new int [b.length() + 1];
        for (int j = 0; j < costs.length; j++)
            costs[j] = j;
        for (int i = 1; i <= a.length(); i++) {
            // j == 0; nw = lev(i - 1, j)
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[b.length()];
    }
}
