package ch.zhaw.ads.hash;

import java.util.*;


public class MyHashtable<K, V> implements java.util.Map<K, V> {
    private K[] keys = null;
    private V[] values = null;
    private boolean isEmpty = true;

    private int hash(Object k) {
        int h = Math.abs(k.hashCode());
        return h % (keys.length);
    }

    public MyHashtable(int size) {
        size *= 2;

        keys = (K[])new Object[size];
        values = (V[])new Object[size];
    }


    //  Removes all mappings from this map (optional operation).
    public void clear() {
        int size = keys.length;
        keys = (K[]) new Object[size];
        values = (V[]) new Object[size];
    }

    //  Returns true if this map contains a mapping for the specified key. 
    public boolean containsKey(Object key) {
        int hashCode = hash(key);

        return keys[hashCode] != null;
    }

    //  Returns true if this map maps one or more keys to the specified value.
    public boolean containsValue(Object value) {
        return values().contains(value);
    }

    //  Returns a set view of the mappings contained in this map.
    public Set entrySet() {
        Set set = new HashSet();
        Map map = new HashMap<K, V>();
        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        set.add(map);
        return set;
    }

    //  Compares the specified object with this map for equality.
    public boolean equals(Object o) {
        throw new UnsupportedOperationException();
    }

    //  Returns the value to which this map maps the specified key.
    public V get(Object key) {
        int index = hash(key);

        while(!keys[index].equals(key)){
            index++;
        }

        return values[index];
    }

    //  Returns the hash code value for this map.
    public int hashCode() {
        throw new UnsupportedOperationException();
    }

    //  Returns true if this map contains no key-value mappings.
    public boolean isEmpty() {
        return isEmpty;
    }

    private boolean isFull(){
        int keylength = keys.length;
        int keytotal = 0;
        for (int i = 0; i < keys.length; i++) {
            if(keys[i] != null) keytotal++;
        }
        return (keytotal >= keylength);
    }

    //  Returns a set view of the keys contained in this map.
    public Set keySet() {
        return new HashSet(Arrays.asList(keys));
    }

    //  Associates the specified value with the specified key in this map (optional operation).
    public V put(K key, V value) {
        isEmpty = false;

        int index = hash(key);

        if (keys[index] != null && keys[index].equals(key)){
            // simply replace
            values[index] = value;
        } else {
            // look for next free position
            while(keys[index] != null && index < keys.length){
                index ++;
            }

            keys[index] = key;
            values[index] = value;
        }

        return value;
    }

    //  Copies all of the mappings from the specified map to this map (optional operation).
    public void putAll(Map t) {
        throw new UnsupportedOperationException();
    }

    //  Removes the mapping for this key from this map if present (optional operation).
    public V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    //  Returns the number of key-value mappings in this map.
    public int size() {
        int count = 0;
        for (int i = 0; i < keys.length; i++) {
            if(keys[i] != null){
                count++;
            }
        }
        return count;
    }

    //  Returns a collection view of the values contained in this map.
    public Collection values() {
        return Arrays.asList(values);
    }

}